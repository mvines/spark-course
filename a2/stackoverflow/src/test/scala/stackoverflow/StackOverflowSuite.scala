package stackoverflow

import org.scalatest.{FunSuite, BeforeAndAfterAll}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import java.io.File

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {


  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

    override def langSpread = 50000

    override def kmeansKernels = 45

    override def kmeansEta: Double = 20.0D

    override def kmeansMaxIterations = 120
  }

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }

  //Test data
  val testPostingsRDD = {
    val testData: Seq[Posting] = Seq(
      Posting(1, 100, None, None, 1, Some("Java")),
      Posting(2, 101, None, Some(100), 4, Some("Java")),
      Posting(2, 102, None, Some(100), 2, Some("Java")),
      Posting(1, 200, None, None, 1, Some("Scala")),
      Posting(2, 201, None, Some(200), 3, Some("Scala"))
    )

    StackOverflow.sc.parallelize(testData)
  }


  val rawRDD: RDD[Posting] = {
    val lines = StackOverflow.sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv")
    testObject.rawPostings(lines).persist()
  }.cache()


  test("groupedPostings should produce correct results") {

    val temp: Set[(Int, Iterable[(Posting, Posting)])] = testObject.groupedPostings(testPostingsRDD).collect().toSet

    // Just converting the 'Iterable' types into 'Set' and 'Seq'
    val result: Set[(Int, Seq[(Posting, Posting)])] = temp.map(elem => (elem._1, elem._2.toSeq))

    val expected: Set[(Int, Seq[(Posting, Posting)])] = Set(
      (100, Seq(
        (Posting(1, 100, None, None, 1, Some("Java")), Posting(2, 101, None, Some(100), 4, Some("Java"))),
        (Posting(1, 100, None, None, 1, Some("Java")), Posting(2, 102, None, Some(100), 2, Some("Java")))
      )),
      (200, Seq(
        (Posting(1, 200, None, None, 1, Some("Scala")), Posting(2, 201, None, Some(200), 3, Some("Scala")))
      ))
    )
    assert(result equals expected, "Expected and result have different elements.")
  }


  test("groupedPostings should not blow up when used with the main data source") {
    assert(testObject.groupedPostings(rawRDD).take(1).headOption.isDefined, "grouped postings is empty")
  }


  test("scoredPostings should produce correct results") {
    val result: Set[(Posting, Int)] = testObject.scoredPostings(testObject.groupedPostings(testPostingsRDD)).collect().toSet

    val expected: Set[(Posting, Int)] =
      Set(
        (Posting(1, 100, None, None, 1, Some("Java")), 4),
        (Posting(1, 200, None, None, 1, Some("Scala")), 3)
      )
  }


  // Whether 'scoredPostings' blows up is also tested here
  test("scoredPostings should have 2121822 questions and no answers in the postings element") {
    val scored = testObject.scoredPostings(testObject.groupedPostings(rawRDD))
    assert(scored.count() === 2121822)

    // Extract only the post ids from the scored rdd above
    val pids = scored.map { case (posting, score) => posting.postingType }.distinct().collect().toSeq
    assert(pids === Seq(1), "pids do have postings that are not queries")
  }


  test("vectorPostings should contain tuples in assignment handout") {

    implicit class CustomSetExtension[A](set: Set[A]) {
      def subsumes(that: Set[A]): Boolean = that.forall(elem => set.contains(elem))
    }

    val vectorRDD: RDD[(Int, Int)] = testObject.vectorPostings(testObject.scoredPostings(testObject.groupedPostings(rawRDD)))
    val result = vectorRDD.collect().toSet

    val expected =
      Set(
        (350000, 67),
        (100000, 89),
        (300000, 3),
        (50000, 30),
        (200000, 20)
      )
    assert(result.subsumes(expected), s"elements are not contained. Result: [${result.mkString(",")}], " +
      s"Expected: ${expected.mkString(",")}")
  }
}
