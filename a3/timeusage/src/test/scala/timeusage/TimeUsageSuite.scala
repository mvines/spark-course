package timeusage

import org.apache.spark.sql.{Column, ColumnName, DataFrame, Row}
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class TimeUsageSuite extends FunSuite with BeforeAndAfterAll {

  import TimeUsage._
  val (dfColumns, dataFrame) = TimeUsage.read("/timeusage/atussum.csv")

  test("dfSchema contains a list where the first field is string and all others are numeric") {
    val testColNameArray = List("field1", "field2", "field3", "field4")
    val expected = StructType(
      List(
        StructField("field1", StringType, nullable = false),
        StructField("field2", DoubleType, nullable = false),
        StructField("field3", DoubleType, nullable = false),
        StructField("field4", DoubleType, nullable = false)
      )
    )
    assert(TimeUsage.dfSchema(testColNameArray) === expected)
  }

  test("dfSchema contains a single field is the field list has only one element") {
    val testColNameArray = List("field1")
    val expected = StructType(List(StructField("field1", StringType, nullable = false)))
    assert(dfSchema(testColNameArray) === expected)
  }

  test("dfSchema has an empty struct field list if the field list is empty") {
    val testColNameArray = List()
    val expected = StructType(List())
    assert(dfSchema(testColNameArray) === expected)
  }

  // TODO Comment test before submitting: The grader may use a different test set, breaking this test
  test("reading the columns of the given dataset must succeed") {
    assert(dfSchema(dfColumns).fields.length === 455)
  }

  // TODO Comment test before submitting: The grader may use a different test set, breaking this test
  test("data frame must return values in the correct type") {
    assert(dataFrame.head().get(0).isInstanceOf[String])
    for (i <- 1 until dfColumns.length) {
      assert(dataFrame.head().get(i).isInstanceOf[Double])
    }
  }

  test("classifiedColumns correctly classifies columns") {

    implicit class ColumnList(val list: List[String]) {
      def toColumn = list.map(c => new Column(c))
    }

    val columnNames = List("t0234sfd", "t0589", "t04dg5", "t06yu", "t01asgr", "t07adfh56", "t08dfwe", "t09we4", "t10sdf4",
      "t1805abc", "t03-wreuy", "t11-akskfj", "t12d23r", "t13fw43", "t1801aslkd", "t1803AJHGDSF", "t14asd3", "t15q23",
      "t16qqd", "t1823567")

    val primaryNeeds = List("t01asgr", "t03-wreuy", "t11-akskfj", "t1801aslkd", "t1803AJHGDSF").toColumn
    val work = List("t0589", "t1805abc").toColumn
    val other = List("t0234sfd", "t04dg5", "t06yu", "t07adfh56", "t08dfwe", "t09we4", "t10sdf4", "t12d23r", "t13fw43",
      "t14asd3", "t15q23", "t16qqd", "t1823567").toColumn

    assert(classifiedColumns(columnNames) === (primaryNeeds, work, other))
  }


}
